package com.herprogramacion.kawaiicards;

import android.util.Log;

/**
 * Creado por Hermosa Programación
 */
public class Carta {

    private int frontImage;
    private int backImage;
    private Estat estat;

    public Carta(int front, int back) {
        this.frontImage = front;
        this.backImage = back;
        this.estat = Estat.BACK;
    }

    public enum  Estat {BACK, FRONT, FIXED}

    public int getImage() {
        int retorno = 0;
        switch (estat) {
            case FIXED:
                retorno = frontImage;
                break;
            case BACK:
                retorno = backImage;
                break;
            case FRONT:
                retorno = frontImage;
                break;


        }

        return retorno;
    }

    public Estat getEstat() {
        return estat;
    }

    public void setEstat(Estat estat) {
        this.estat = estat;
    }

    public void girar() {
        switch (estat) {
            case FIXED:
                estat = Estat.FIXED;
                break;
            case BACK:
                estat = Estat.FRONT;
                break;
            case FRONT:
                estat = Estat.BACK;
                break;
        }



    }
}
