package com.herprogramacion.kawaiicards;


import java.util.List;

public class Joc {

    List<Carta> cartas;
    boolean esPotJugar = true;

    public Joc(List<Carta> cartas) {
        this.cartas = cartas;
    }

    public boolean isEsPotJugar() {
        return esPotJugar;
    }

    public void setEsPotJugar(boolean esPotJugar) {
        this.esPotJugar = esPotJugar;
    }

    public boolean guanyat(){
        this.setEsPotJugar(false);
        boolean guanyat = true;
        for (Carta carta : this.cartas){
            if (carta.getEstat() != Carta.Estat.FIXED){
                guanyat = false;
            }else if (carta.getEstat() == Carta.Estat.FRONT){
                carta.setEstat(Carta.Estat.BACK);
            }
        }
        return  guanyat;
    }
}
