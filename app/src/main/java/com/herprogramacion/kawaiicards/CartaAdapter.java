package com.herprogramacion.kawaiicards;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import java.util.List;



public class CartaAdapter extends RecyclerView.Adapter<CartaAdapter.CartaViewHolder> {
    private List<Carta> cartas;
    private Context context;
    private int destapada = 0;
    private int girada1, girada2;
    private Chronometre crono;


    public class CartaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView imagen;

        public CartaViewHolder(View itemView) {
            super(itemView);
            imagen = (ImageView) itemView.findViewById(R.id.imagen);
            imagen.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (Integer.valueOf(crono.tvTemps.getText().toString()) > 0) {
                if (destapada <= 1) {
                    if (cartas.get(getPosition()).getEstat() == Carta.Estat.BACK) {
                        if (destapada == 0) {
                            cartas.get(getPosition()).girar();
                            notifyDataSetChanged();
                            girada1 = this.getPosition();
                            destapada = 1;
                        } else if (destapada == 1) {
                            cartas.get(getPosition()).girar();
                            notifyDataSetChanged();
                            girada2 = this.getPosition();
                            destapada = 2;
                            if (cartas.get(girada1).getImage() == cartas.get(girada2).getImage()) {
                                cartas.get(girada1).setEstat(Carta.Estat.FIXED);
                                cartas.get(girada2).setEstat(Carta.Estat.FIXED);
                                destapada = 0;
                            } else {
                                final int value = this.getPosition();
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        cartas.get(girada1).girar();
                                        cartas.get(girada2).girar();
                                        notifyDataSetChanged();
                                        destapada = 0;
                                    }
                                }, 2000);
                            }
                        }
                    }
                }
            }
        }
    }

    public CartaAdapter(List<Carta> cartas, Chronometre crono) {
        this.cartas = cartas;
        this.crono = crono;
    }

    @Override
    public CartaAdapter.CartaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.anime_card, parent, false);
        return new CartaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CartaAdapter.CartaViewHolder holder, int position) {
        holder.imagen.setImageResource(cartas.get(holder.getPosition()).getImage());
    }

    @Override
    public int getItemCount() {
        return cartas.size();
    }
}
