package com.herprogramacion.kawaiicards;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private ListView lvMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvMenu = findViewById(R.id.lvMenu);
        lvMenu.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


        switch (i) {
            case 0:
                Intent facil = new Intent(this, NivellFacil.class);
                startActivity(facil);
                break;
            case 1:
                Intent mig = new Intent(this, NivellMitja.class);
                startActivity(mig);
                break;
            case 2:
                Intent dificil = new Intent(this, NivellDificil.class);
                startActivity(dificil);

                break;
        }
    }
}
