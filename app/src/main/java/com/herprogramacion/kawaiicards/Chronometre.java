package com.herprogramacion.kawaiicards;

import android.os.CountDownTimer;
import android.widget.TextView;

public class Chronometre extends CountDownTimer {

    public TextView tvTemps;
    public TextView tvGuanyat;
    Joc joc;

    public Chronometre(long millisInFuture, long countDownInterval, TextView textView, TextView textView1, Joc joc) {
        super(millisInFuture, countDownInterval);
        tvTemps = textView;
        tvGuanyat = textView1;
        this.joc = joc;
    }

    @Override
    public void onTick(long l) {

        if (l > 0 && joc.guanyat()) {
            tvTemps.setText("0");
            tvGuanyat.setText("Enhorabona!");
        }else{
            tvTemps.setText("" + l / 1000);
        }

    }

    @Override
    public void onFinish() {
        if (joc.guanyat()){
            tvGuanyat.setText("Has guanyat!!! :)");
        }else{
            tvGuanyat.setText("Has perdut :(");
        }
    }
}
