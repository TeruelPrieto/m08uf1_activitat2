package com.herprogramacion.kawaiicards;


import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



public class NivellFacil extends ActionBarActivity {

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;
    Joc joc;
    private TextView tvTemps;
    private TextView tvGuanyat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        tvTemps = findViewById(R.id.tvtemps);
        tvGuanyat = findViewById(R.id.tvGuanyat);
        List<Carta> cartas = new ArrayList<>();

        cartas.add(new Carta(R.drawable.c6, R.drawable.back));
        cartas.add(new Carta(R.drawable.c7, R.drawable.back));
        cartas.add(new Carta(R.drawable.c8, R.drawable.back));
        cartas.add(new Carta(R.drawable.c9, R.drawable.back));
        cartas.add(new Carta(R.drawable.c10, R.drawable.back));
        cartas.add(new Carta(R.drawable.c11, R.drawable.back));
        cartas.add(new Carta(R.drawable.c6, R.drawable.back));
        cartas.add(new Carta(R.drawable.c7, R.drawable.back));
        cartas.add(new Carta(R.drawable.c8, R.drawable.back));
        cartas.add(new Carta(R.drawable.c9, R.drawable.back));
        cartas.add(new Carta(R.drawable.c10, R.drawable.back));
        cartas.add(new Carta(R.drawable.c11, R.drawable.back));

        Collections.shuffle(cartas);

        joc = new Joc(cartas);

       Chronometre crono = (Chronometre) new Chronometre(100000, 1000, tvTemps, tvGuanyat, joc) {
        }.start();


        recycler = (RecyclerView) findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);

        lManager = new GridLayoutManager(this,3 );
        recycler.setLayoutManager(lManager);

        adapter = new CartaAdapter(cartas, crono);
        recycler.setAdapter(adapter);
    }
}

